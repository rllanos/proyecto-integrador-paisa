/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Maria Guerra
 */
public class Consultas extends conexion {
    
    public boolean autenticacion (String Usuario, String Contraseña ){
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            String Consultas = " select * from registro where Usuario = ? and Password = ? ";
            pst = getconexion().prepareStatement(Consultas);
            pst.setString(1, Usuario);
            pst.setString(2, Contraseña);
            rs = pst.executeQuery();
            
            if (rs.absolute(1)){
                return true;
            }
                
        } catch (Exception e) {
            System.err.println(" Error " + e);            
        } finally {
            try {
                if (getconexion()!=null)getconexion().close();
                if (pst!=null)pst.close();
                if (rs !=null)rs.close();
                
            } catch (Exception e) {
                System.err.println(" Error " + e);
            }
            
        }        
        
        return false;
    }
   public boolean Registrar (String Nombre_usua, String Apellido_usua, String Usuario, String Contraseña ){
      PreparedStatement pst = null;
       try {
           String Consultas = "insert into usuario (Nombre_usua,Apellido_usua,Usuario,Password) values (?,?,?,?)";
           pst = getconexion().prepareStatement(Consultas);
           pst.setString(1, Nombre_usua);
           pst.setString(2, Apellido_usua);
           pst.setString(3, Usuario);
           pst.setString(4, Contraseña);
           
           if(pst.executeUpdate()==1){
               return true;
           }
           
       } catch (Exception ex) {
           System.err.println("Error"+ex);
       }finally {
           try {
               if (getconexion()!=null)getconexion().close();
               if (pst !=null)pst.close();
           } catch (Exception e) {
               System.err.println("Error"+e);
           }
       }
       
       return false;
    }
   
 
    }

